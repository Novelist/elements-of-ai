import random
def main():

    prob = random.random()
    if prob > 0.2:
        favourite = 'dogs'
    elif prob > 0.1:
        favourite = 'cats'
    else:
        favourite = 'bats'

    print("I love " + favourite)

main()
