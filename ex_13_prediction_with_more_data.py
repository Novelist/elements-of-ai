import numpy as np
from io import StringIO

input_string = '''
25 2 50 1 500 127900
39 3 10 1 1000 222100
13 2 13 1 1000 143750
82 5 20 2 120 268000
130 6 10 2 600 460700
115 6 10 1 550 407000
'''

np.set_printoptions(precision=1)  # this just changes the output settings for easier reading

def fit_model(input_file):
    # Please write your code inside this function
    data = np.genfromtxt(input_file)
    x = np.delete(data, (len(data[0])-1), axis=1)
    y = np.delete(data, range(len(data[0])-1), axis=1)

    # this new array is because of AssertionFailed of the unit test.
    # y has arrays in arrays with only one value, so get rid of it by looping over it and
    # only appending the value to a new array
    y_new = []
    for i in range(len(y)):
        y_new.append(y[i][0])
    # read the data in and fit it. the values below are placeholder values
    c = np.linalg.lstsq(x, y_new, rcond=None)[0]  # coefficients of the linear regression

    print(c)
    print(x @ c)


# simulate reading a file
input_file = StringIO(input_string)
fit_model(input_file)
