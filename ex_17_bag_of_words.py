import numpy as np

data = [[1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1],
        [1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1],
        [1, 1, 1, 0, 1, 3, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1]]

def find_nearest_pair(data):
    N = len(data)
    dist = np.empty((N, N), dtype=float)

    for i in range(len(dist)):
        dist_val = 0
        idx = 0
        while idx < len(dist):
            for j in range(len(dist[i])):
                dist_val += abs(data[i][j]-data[idx][j])
                if j == len(dist[i])-1:
                    if i == idx:
                        dist[i][idx] = np.inf
                    else:
                        dist[i][idx] = dist_val
                    dist_val = 0
                    idx += 1

    print(np.unravel_index(np.argmin(dist), dist.shape))

find_nearest_pair(data)
