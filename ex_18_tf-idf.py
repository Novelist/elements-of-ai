import math
import numpy as np

text = '''Humpty Dumpty sat on a wall
Humpty Dumpty had a great fall
all the king's horses and all the king's men
couldn't put Humpty together again'''


def main(text):
    # tasks your code should perform:

    # 1. split the text into words, and get a list of unique words that appear in it
    # a short one-liner to separate the text into sentences (with words lower-cased to make words equal
    # despite casing) can be done with
    docs = [line.lower().split() for line in text.splitlines()]
    words = list(set(text.lower().split()))

    # 2. go over each unique word and calculate its term frequency, and its document frequency
    df = {}
    tf = {}
    for word in words:
        df[word] = sum([word in doc for doc in docs])/len(docs)
        tf[word] = [doc.count(word)/len(doc) for doc in docs]

    # 3. after you have your term frequencies and document frequencies, go over each line in the text and
    # calculate its TF-IDF representation, which will be a vector
    tfidf = []
    for index, doc in enumerate(docs):
        value = []
        for word in words:
            value.append((tf[word][index] * math.log(df[word],10)))
        tfidf.append(value)

    # 4. after you have calculated the TF-IDF representations for each line in the text, you need to
    # calculate the distances between each line to find which are the closest.
    dist = np.empty((len(docs),len(docs)), dtype=float)
    for sent1 in range(len(dist)):
        for sent2 in range(len(dist)):
            if sent1 == sent2:
                dist[sent1][sent2] = np.inf
            else:
                dist[sent1][sent2] = abs(sum(tfidf[sent1])-sum(tfidf[sent2]))

    print(np.unravel_index(np.argmin(dist), dist.shape))


main(text)
